//This is function creation

//THis code fetches the buttons from HTML and assign them to variables
let button1 = document.getElementById("b1");
let button2 = document.getElementById("b2");
let button3 =document.getElementById("b3");

function doSomething1()
{ 
  alert("I love HTML,CSS JS");
}

function doSomething2()
{
    alert("I cant wait to start semester 2");
}

function doSomething3()
{
    alert("I want to develop a large web application");
}



/*Makes the buttons an event listener that listen 
to a click and calls a particular function to be
executed when the event occurs
*/
button1.addEventListener("click",doSomething1);
button2.addEventListener("click",doSomething2);
button3.addEventListener("click",doSomething3);

